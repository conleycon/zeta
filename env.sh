curdir=$(dirname $BASH_SOURCE)

. $curdir/.nvm/nvm.sh

export PATH=$(npm bin):$PATH
# remove duplicate entries
export PATH=$(printf %s "$PATH" | awk -v RS=: -v ORS=: '!arr[$0]++')

echo "Ready to rock and roll!"
