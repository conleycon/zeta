This runs in MicroPython on one (or more) ESP8266 modules. It lets you run 
Zeta game control buttons on tiny cheap WiFi microcontrollers. You would 
connect GPIO0 to e.g. one of those Staples Easy Buttons or something.

Steps:
1 Use esptool to install the MicroPython firmware.
2 Use the serial REPL to start webREPL and set a password.
3 Modify config-1 and config-2 to taste.
4 Upload boot.py and main.py on both micros
5 Upload config-1 to the micro you're using for the start/stop button
6 Upload config-2 to the micro you're using for the objective.
7 Reset everything
