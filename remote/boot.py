#! /usr/bin/env python

import network,time
import config

# Disable the AP interface
network.WLAN(network.AP_IF).active(False)

# Connect to the network
wifi = network.WLAN(network.STA_IF)
wifi.active(True)
wifi.scan()
wifi.connect(config.ssid,config.psk)
while not wifi.isconnected():
    print("Waiting for WiFi connection...")
    time.sleep(0.5)

