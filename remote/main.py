#! /usr/bin/env python

import machine
import time
import socket
import config

button = machine.Pin(0)
triggered = False

def irqCB(p):
    global triggered
    triggered=True

button.irq(trigger=machine.Pin.IRQ_FALLING,handler=irqCB)

while True:
    if triggered:
        print("button press")
        triggered = False
        sock = socket.socket()
        try:
            sock.connect(socket.getaddrinfo(config.host,config.port)[0][-1])
            sock.send("GET " + config.path + " HTTP/1.1\n\n")
            print(sock.recv(1000))
        except OSError as e:
            print(repr(e))
    machine.idle()
    time.sleep(0.1)
    
