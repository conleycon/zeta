#!/usr/bin/env node
'use strict';

const zetaserver = require('./lib/server.js');
const zetagame = require('./lib/game.js');
const zetasensor = require('./lib/sensor.js');

const PINS = [
    {
        laser: 17,
        led: 4
    },
    {
        laser: 27,
        led: 23
    },
    {
        laser: 22,
        led: 24
    }
]

const lasers = []
function setupHardware(game) {
    PINS.forEach(cfg => lasers.push(new zetasensor.Sensor(cfg.laser, cfg.led)))
    console.log(lasers)
    lasers.forEach(laser => laser.on(value => game.laserStateChange(value)))

    function shutdown() {
        lasers.forEach(laser => laser.close())
    }

    process.on('SIGINT', shutdown)
}

async function main() {
    const game = new zetagame.Game();
    try {
        setupHardware(game);
    } catch (e) {
        console.error(e);
    }

    try {
        const server = new zetaserver.StatServer(
                'data.json',
                () => game.pressFirstButton(),
                () => game.pressSecondButton(),
                () => game.sync());
        game.setClock(server.clock());
        await server.start();
    } catch (e) {
        console.error(e);
    }
}

main();
