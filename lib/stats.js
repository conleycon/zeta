'use strict';


const kAddPersonValue = 'Add new person';
const kEmptyPersonValue = '-----';


// BEGIN utilities ////////////////////////////////////////////////////////////

function makeOptionNode(value) {
    const node = document.createElement('option');
    node.value = value;
    node.textContent = value;
    return node;
}


// BEGIN classes //////////////////////////////////////////////////////////////

class Person {
    constructor(name) {
        this._name = name;
    }

    name() {
        return this._name;
    }

    // BEGIN client-specific code /////////////////////////////////////////////
    optionNode() {
        if (!this._optionNode) {
            this._optionNode = makeOptionNode(this._name);
        }
        return this._optionNode;
    }
}

class PersonList {
    constructor() {
        this._persons = [];
        this._activePerson = null;
        this._ws = null;
    }

    send(obj) {
        this._ws.send(JSON.stringify(obj));
    }

    addPerson(name) {
       const person = new Person(name);
       this._persons.push(person);
       return person;
    }

    getPerson(name) {
        for (let person of this._persons) {
            if (person.name() === name) {
                return person;
            }
        }
        return null;
    }

    hasPerson(name) {
        return this.getPerson(name) !== null;
    }

    setActivePerson(name) {
        this._activePerson = this.getPerson(name);
        if (this._activePerson === null) {
            throw `No such person: ${name}`;
        }
        if (this._select) {
            this._select.value = name;
        }
    }

    getData() {
        const data = {
            persons: [],
        }
        for (let person of this._persons) {
            data.persons.push(person.name());
        }
        return data;
    }

    setData(data) {
        for (let name of data.persons) {
            this.addPerson(name);
        }
    }

    // BEGIN server-specific code /////////////////////////////////////////////

    // BEGIN client-specific code /////////////////////////////////////////////
    setSelect(select) {
        this._select = select;
        this._group = this._select.querySelector('optgroup');
        const personList = this;
        this._select.addEventListener('change', () => {
            if (this._select.value === kAddPersonValue) {
                personList.create();
            } else {
                personList.setActivePerson(this._select.value);
            }
        });
    }

    seed(data) {
        this.setData(data);
        for (let person of this._persons) {
            this._group.appendChild(person.optionNode());
        }
        if (this._persons.length === 0) {
            this._emptyPersonNode = makeOptionNode(kEmptyPersonValue);
            this._group.appendChild(this._emptyPersonNode);
        }
        this._newPersonNode = makeOptionNode(kAddPersonValue);
        this._group.appendChild(this._newPersonNode);
    }

    setWs(ws) {
        this._ws = ws;
        //ws.onmessage = (event) => {
        //    const data = JSON.parse(event.data);
        //    console.log(`Received ${data.event}`);
        //    switch (data.event) {
        //        case 'seed':
        //            this.seed(data.data);
        //            break;
        //        default:
        //            throw `unhandled event ${data.event}`;
        //    }
        //};
    }

    create() {
        const name = prompt('Enter your name');
        if (this.hasPerson(name)) {
            alert(`${name} already exists.`);
            return;
        }
        if (!name) {
            alert('Empty names not allowed!');
            return;
        }
        if (this._emptyPersonNode) {
            this._group.removeChild(this._emptyPersonNode);
            this._emptyPersonNode = null;
        }
        const person = this.addPerson(name);
        this._group.insertBefore(person.optionNode(), this._newPersonNode);
        this.setActivePerson(name);
        this.send({
            event: 'addPerson',
            name: name,
        });
    }
}

class Clock {
    constructor(node, ws) {
        this._node = node;
        const clock = this;
        this._ws = ws;
        ws.onmessage = event => {
            const data = JSON.parse(event.data);
            console.log(`Received ${data.event}`);
            switch(data.event) {
                case 'start':
                    const time = data.time ? data.time : 0;
                    clock.start(time);
                    break;
                case 'update':
                    clock.update(data.time);
                    break;
                case 'stop':
                    clock.stop(data.time);
                    break;
                default:
                    throw `unhandled event ${data.event}`;
            }
        };
        this._running = false;
        this.update(0);
        
        setInterval(() => clock.tick(), 100);
    }

    start(time) {
        this._running = true;
        this.update(time);
    }

    update(time) {
        this._time = time;
        const minutes = String(Math.floor(time / 60000)).padStart(2, '0');
        time %= 60000;
        const seconds = String(Math.floor(time / 1000)).padStart(2, '0');
        time %= 1000;
        const ms = String(Math.floor(time / 100));
        this._node.textContent = `${minutes}:${seconds}.${ms}`
        this._last = new Date().getTime();
    }

    tick() {
        if (!this._running) return;
        const newMs = new Date().getTime() - this._last;
        this.update(this._time + newMs);
    }

    stop(time) {
        this._running = false;
        this.update(time);
    }

    sync() {
        this._ws.send(JSON.stringify({
            event: 'sync',
        }));
    }
}


// BEGIN exports //////////////////////////////////////////////////////////////

module.exports.Clock = Clock;
module.exports.Person = Person;
module.exports.PersonList = PersonList;
