'use strict'

class Game {
    constructor() {
      this.reset();
    }

    reset() {
      this.startTime = new Date();
      this.endTime = 0;
      this.stage = 0;
      this.blockStart = 0;
      this.gracePeriodStart = 0;
      this.laserTriggers = 0;
      this.visible = true;
    }

    sync() {
      this.clock.start()
      switch (this.stage) {
        case 0:
        case 3:
          this.clock.stop(this.getCurrentScore());
          break;
        case 1:
        case 2:
          this.clock.start(this.getCurrentScore());
          break;
      }
    }

    setClock(clock) {
      this.clock = clock;
    }

    pressFirstButton() {
      console.log(`First button pushed in stage ${this.stage}`)
      switch(this.stage) {
          case 0:
          case 3:
            if (new Date() - this.endTime < 1000) {
              console.log("tried to start too soon")
              return;
            }
            this.reset();
            this.stage = 1
            this.clock.start();
            break;
          case 2:
            this.stage = 3;
            this.endTime = new Date();
            const score = this.getCurrentScore()
            console.log(score)
            this.clock.stop(score);
            break;
      }
      console.log(`...Stage is now ${this.stage}`)
    }

    pressSecondButton() {
      console.log(`Second button pushed in stage ${this.stage}`)
      switch (this.stage) {
          case 1:
            this.stage = 2;
            break;
      }
      console.log(`...Stage is now ${this.stage}`)
    }

    isGracePeriod() {
      const value = (new Date().getTime() - this.gracePeriodStart) < 1000;
      return value;
    }

    laserStateChange(visible) {
      if(visible == this.visible){
        console.warn(`laserStateChange called, but state is still ${visible}`)
        return;
      }
      this.visible = visible;
      console.log(`Set visible to ${visible}`)

      if(this.visible){
        this._laserUnblocked()
      } else{
        this._laserBlocked()
      }
    }

    _laserBlocked() {
      console.log("laser is blocked")
      if (this.isGracePeriod()) {
        this.blockStart = new Date(this.gracePeriodStart.getTime() + 1000);
      } else{
        this.blockStart = new Date();
      }

      console.log(`blockStart set to ${this.blockStart}`)
    }

    _laserUnblocked() {

      if (this.stage == 0 || this.stage == 3) {
        return;
      }

      console.log("laser is unblocked")
      if (this.isGracePeriod()) {
        console.log("in grace period. returning.")
        return;
      }

      this.gracePeriodStart = new Date();
      const newTriggers = Math.ceil((this.gracePeriodStart.getTime() - this.blockStart.getTime()) / 1000);
      this.laserTriggers += newTriggers;
      console.log(`Adding ${newTriggers} laser triggers`)
      this.clock.update(this.getCurrentScore());

      console.log(`gracePeriodStart set to ${this.gracePeriodStart}`)
      console.log(`laserTriggers set to ${this.laserTriggers}`)
    }

    getCurrentScore() {
      return this.getScore(new Date());
    }

    getScore(time) {
      const totalTime = time.getTime() - this.startTime.getTime();
      const penalty = this.laserTriggers * 30000;
      const score = totalTime + penalty;
      console.log(`Score is ${score} with ` +
                  `${this.laserTriggers} laser triggers`);
      return score
    }
}

module.exports.Game = Game;
