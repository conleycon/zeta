'use strict';

const express = require('express');
const expressWs = require('express-ws');
const fs = require('fs');
const path = require('path');
const browserify = require('browserify');
const streamToString = require('stream-to-string')
const stats = require('./stats.js')
const WebSocket = require('ws');

const kRootPath = path.join(__dirname, '..');
const kResPath = path.join(kRootPath, 'res');
const kPort = 9001;


// BEGIN requests /////////////////////////////////////////////////////////////

function fileRequest(relPath) {
    return (req, res) => {
        const filePath = path.join(kResPath, relPath);
        const data = fs.readFileSync(filePath, 'utf-8');
        res
            .contentType(path.basename(relPath))
            .status(200)
            .send(data);
    }
}


let mainJsPromise = null;
async function getMainJs() {
    if (mainJsPromise === null) {
        mainJsPromise = streamToString(browserify({
            debug: true,
        })
            .add(path.join(kResPath, 'main.js'))
            .bundle());
    }
    return await mainJsPromise;
}

class ServerClock {
    constructor(wsList) {
        this._wsList = wsList;
    }

    send(data) {
        for (let ws of this._wsList) {
            if (ws.readyState === WebSocket.OPEN) {
                ws.send(JSON.stringify(data));
            }
        }
    }

    start(time) {
        this.send({
            event: 'start',
            time,
        });
    }

    update(time) {
        this.send({
            event: 'update',
            time,
        });
    }

    stop(time) {
        this.send({
            event: 'stop',
            time,
        });
    }
}


// BEGIN Server ///////////////////////////////////////////////////////////////

class StatServer {
    constructor(path, button1Func, button2Func, syncFunc) {
        this._app = express();
        this._appWs = expressWs(this._app);
        this._path = path;
        this._personList = null;
        this._button1Func = button1Func;
        this._button2Func = button2Func;
        this._syncFunc = syncFunc;

        this._app.ws('/clock', (ws, req) => {
            console.log('Connection established for /clock');
            ws.onmessage = (event) => {
                const data = JSON.parse(event.data);
                switch(data.event) {
                    case 'sync':
                        this._syncFunc();
                        break;
                    default:
                        throw `Unhandled event "${data.event}"`;
                }
            };
        });
        this._clock = new ServerClock(this._appWs.getWss('/clock').clients);
    }

    clock() {
        return this._clock;
    }

    syncToFile() {
        const data = JSON.stringify(this._personList.getData());
        fs.writeFileSync(this._path, data);
    }

    async start() {
        let data;
        try {
            const fileData = fs.readFileSync(this._path, 'utf-8');
            data = JSON.parse(fileData);
        } catch (e) {
            console.error(e);
            data = {
                persons: [],
            };
        }

        const personList = new stats.PersonList();
        personList.setData(data);
        this._personList = personList;

        this._app.ws('/persons', (ws, req) => {
            console.log('Connection established for /persons');
            ws.onmessage = (event) => {
                const data = JSON.parse(event.data);
                switch(data.event) {
                    case 'addPerson':
                        this._personList.addPerson(data.name);
                        this.syncToFile();
                        break;
                    default:
                        throw `Unhandled event "${data.event}"`;
                }
            };
            ws.send(JSON.stringify({
                event: 'seed',
                data: personList.getData(),
            }));
        });

        const mainJs = await getMainJs();
        const button1Func = this._button1Func;
        const button2Func = this._button2Func;
        this._app.get('/', fileRequest('index.html'));
        this._app.get('/button1', (req, res) => {
            button1Func();
            res.status(200).send();
        });
        this._app.get('/button2', (req, res) => {
            button2Func();
            res.status(200).send();
        });
        this._app.get('/main.js', (req, res) => {
            res.contentType(path.basename('main.js'))
                .status(200)
                .send(mainJs);
        });
        this._app.get('/main.css', fileRequest('main.css'));
        this._app.listen(kPort, () => {
            console.log(`Serving at http://0.0.0.0:${kPort}`);
        });
    }
}


// BEGIN exports //////////////////////////////////////////////////////////////

module.exports.StatServer = StatServer;
