"use strict";

const Gpio = require('onoff').Gpio

class Sensor {
    constructor(sensorPin, ledPin) {
        this.sensor = new Gpio(sensorPin, 'in', 'both')
        this.led = new Gpio(ledPin, 'out')
    }

    on(cb) {
        this.sensor.watch((err, value) => {
            this.led.writeSync(value);
            if (err) {
                console.error("Error: ", err)
            }
            cb(value)
        })
    }

    close() {
      	this.led.writeSync(0);
      	this.led.unexport();
        this.sensor.unexport()
    }
}

module.exports.Sensor = Sensor
