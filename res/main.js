'use strict';

const stats = require('../lib/stats.js');

function createWs(path) {
    let ws = new WebSocket(`ws://${location.host}/${path}`);

    ws.onerror = event => {
        console.log(`Websocket error: error ${event.type}`);
    };

    ws.onopen = event => {
        console.log('Connection established');
    };
    
    ws.onclose = event => {
        console.log('Connection lost');
        ws = null;
    };
    return ws;
}

document.addEventListener('DOMContentLoaded', () => {
    const personList = new stats.PersonList();
    personList.setWs(createWs('persons'));
    personList.setSelect(document.querySelector('#runner select'));

    const clockNode = document.querySelector('#clock span');
    const clock = new stats.Clock(clockNode, createWs('clock'));
});
