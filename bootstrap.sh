#!/bin/bash
# Installs necessary development tools.
# In theory, this script should not touch the network unless it actually needs
# to install something.
set -e

# Newest versions may be determined by:
# (cd .nvm; git describe --abbrev=0 --tags --match "v[0-9]*" origin)
NVM_VERSION=v0.33.1
# Newest versions may be determined at:
# https://nodejs.org/en/download/releases/
# Prefer LTS releases.
NODE_VERSION=8.11.1


setup=$(basename $0)
curdir=$(dirname $0)

progress() {
    echo "[$setup] $@..."
}

# Setup nvm
progress "Installing nvm"
nvmdir=$curdir/.nvm
echo nvmdir $nvmdir
if [ ! -d $nvmdir ]
then
    git clone https://github.com/creationix/nvm.git $nvmdir
fi
(
    cd $nvmdir
    git cat-file -t $NVM_VERSION || git fetch
    git checkout $NVM_VERSION
)
. $nvmdir/nvm.sh
nvm install $NODE_VERSION
nvm alias default $NODE_VERSION
nvm use $NODE_VERSION


# TODO: I'm not sure any of this is needed
# Install node packages
progress "Installing node packages"
npm install


echo 'Run `. env.sh`'
